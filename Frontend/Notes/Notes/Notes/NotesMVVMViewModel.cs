﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace Notes
{
    public class NotesMVVMViewModel : INotifyPropertyChanged
    {
        public string NotesDisplay { get; set; }
        public string editor { get; set; }
        public string Save { get; set; }
        public string Delete { get; set; }

        private string _fileName  { get; set; }

        public ICommand SaveButton { get; set; }
        public ICommand DeleteButton { get; set; }

        public NotesMVVMViewModel ()
        {
            NotesDisplay = "Notes";
            Save = "Save";
            Delete = "Delete";

            _fileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "notes.txt");
            if (File.Exists(_fileName))
            {
                EditorText = File.ReadAllText(_fileName);
            }

            SaveButton = new Command(() => SaveTexte());
            DeleteButton = new Command(() => DeleteTexte());
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public string EditorText
        {
            set
            {
                editor = value;
                OnPropertyChanged("EditorText");
            }
            get { return editor; }
        }

        
        private void SaveTexte ()
        {
            File.WriteAllText(_fileName, EditorText);
        }

        private async void DeleteTexte()
        {
            
            bool result = await App.Current.MainPage.DisplayAlert("CONFIRMATION", "Voulez vous vraiment supprimer le texte ?", "Oui", "Non");

            if (result)
            {
                if (File.Exists(_fileName))
                {
                   File.Delete(_fileName);
                }
                EditorText = string.Empty;
            }
            else
            {
                return;
            }

            
        }

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
