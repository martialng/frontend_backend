﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace EFGetStarted
{
    public class BloggingContext : DbContext
    {
        public DbSet<Blog> Blogs { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Auteur> Auteurs { get; set; }
        public DbSet<Commentaire> Commentaires { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlite("Data Source=blogging.db");
    }

    public class Blog
    {
        public int BlogId { get; set; }
        public string Url { get; set; }

        public List<Post> Posts { get; } = new List<Post>();
    }

    public class Auteur
    {
        public int AuteurId { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string Photo { get; set; }

        public List<Post> Posts { get; } = new List<Post>();
        public List<Commentaire> Commentaires { get; } = new List<Commentaire>();
    }

    public class Post
    {
        public int PostId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }

        public int BlogId { get; set; }
        public Blog Blog { get; set; }
        public int AuteurId { get; set; }
        public Auteur Auteur { get; set; }

        public List<Commentaire> Commentaires { get; } = new List<Commentaire>();
    }

    public class Commentaire
    {
        public int CommentaireId { get; set; }
        public string LibelleComment { get; set; }

        public int PostId { get; set; }
        public Post Post { get; set; }
        public int AuteurId { get; set; }
        public Auteur Auteur { get; set; }
    }
}
