﻿using System;
using System.Linq;

namespace EFGetStarted
{
    class Program
    {
        static void Main()
        {
            using (var db = new BloggingContext())
            {
                // Create
                Console.WriteLine("Inserting a new blog");
                db.Add(new Blog { Url = "http://blogs.msdn.com/adonet" });
                db.SaveChanges();

                Console.WriteLine("Inserting a first Auteur");
                db.Add(new Auteur { Nom = "Jardow", Prenom = "Jardy", Photo = "Jardow qaaas" });
                db.SaveChanges();
                var autA = db.Auteurs.OrderByDescending(b => b.AuteurId).First();

                Console.WriteLine("Inserting a second Auteur");
                db.Add(new Auteur { Nom = "Matins", Prenom = "Jordy", Photo = "Photo Tata tonton" });
                db.SaveChanges();
                var autB = db.Auteurs.OrderByDescending(b => b.AuteurId).First();

                // Read
                Console.WriteLine("Querying for a blog");
                var blog = db.Blogs
                    .OrderBy(b => b.BlogId)
                    .First();

                // Update
                Console.WriteLine("Updating the blog and adding a post");
                blog.Url = "https://devblogs.microsoft.com/dotnet";
                blog.Posts.Add(
                    new Post
                    {
                        Title = "Hello World",
                        Content = "I wrote an app using EF Core!",
                        Auteur = autA
                    });
                db.SaveChanges();

                // Read
                Console.WriteLine("Querying for a post");
                var post = db.Posts
                    .OrderBy(b => b.PostId)
                    .First();

                // Update
                Console.WriteLine("Updating the post and adding a commentaires");
                post.Content = "I wrote an app ";
                post.Commentaires.Add(
                    new Commentaire
                    {
                        LibelleComment = "Un Article correct",
                        Auteur = autA
                    });
                post.Commentaires.Add(
                    new Commentaire
                    {
                        LibelleComment = "Un Article moins bon",
                        Auteur = autB
                    });
                post.Commentaires.Add(
                    new Commentaire
                    {
                        LibelleComment = "Le poste est bien lisible",
                        Auteur = autB
                    });
                db.SaveChanges();

                // Delete
                Console.WriteLine("Delete the blog");
                db.Remove(blog);
                db.SaveChanges();
            }
        }
    }
}